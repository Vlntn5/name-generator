const express = require('express')
const path = require('path')
const webpush = require('web-push')
const fs = require('fs')
const port = process.env.PORT || 8080

const app = express()

//inject random name into html
app.get('/', function(req, res) {
    var data = fs.readFileSync('./public/index.html', 'utf-8')
    var names = require('./public/names.json')

    var ind = Math.floor(Math.random() * names.length)
    data = data.replace('$RANDOM_NAMES', names[ind])

    res.setHeader('Content-Type','text/html')
    res.send(data)
})

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.post('/sendName', function(req, res) {
    var file = require('./public/names.json')
    file.push(req.body.name)
    fs.writeFile('./public/names.json', JSON.stringify(file), (err) => {
        if(!err) console.log("Writting to file..")
    })

    sendPushNotifications(req.body.name)
    res.json({
        id: req.body.id
    })
})

app.post('/sendNameOld', function(req, res) {
    var file = require('./public/names.json')
    file.push(req.body.name)
    fs.writeFile('./public/names.json', JSON.stringify(file), (err) => {
        if(!err) console.log("Writting to file..")
    })

    sendPushNotifications(req.body.name)
    res.redirect('/')
})


let subscriptions = [];

app.post("/saveSubscription", function(req, res) {
    subscriptions = subscriptions.filter(x => {x.keys.p256dh != req.body.sub.keys.p256dh})
    subscriptions.push(req.body.sub);
    console.log("Saving a subscription, current status :: ", subscriptions)
    res.sendStatus(200);
});

app.post("/deleteSubscription", function(req, res) {
    subscriptions = subscriptions.filter(x => {x.keys.p256dh != req.body.sub.keys.p256dh})
    console.log("Deleting a subscription, current status :: ", subscriptions)
    res.sendStatus(200);
});

async function sendPushNotifications(name) {
    webpush.setVapidDetails('mailto:valentin.jukanovic@fer.hr', 
    'BDMFSdlI3B9diMlf-L-_dKOyc30oOKtXKfgSyDRV80El4WZEuwbjJPyM22LPWWgfrx5YRxBAlPdI0SmSKgTR-8o', 
    'cN1r9-1Tw_fiOOSmiVmu0MxFsmDH6Pvigj8-p3hRcdQ');
    subscriptions.forEach(async sub => {
        try {
            console.log("Sending notif to", sub);
            await webpush.sendNotification(sub, JSON.stringify({
                title: 'RNG - Notification',
                body: `Hi there, we have a new name in database ${name}`,
                redirectUrl: '/'
              }));    
        } catch (error) {
            console.error(error);
        }
    });
}

app.listen(port, function() {
    console.log(`App listening on port ${port}`)
})