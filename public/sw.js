importScripts('./idb-keyval.js')

const cacheName = "generator-assets-v1"
const cachables = [
    "./",
    "./manifest.webmanifest",
    "./index.html",
    "./main.js",
    "./style.css",
    "./names.json",
    "./idb-keyval.js",
    "./offline404.html",
    "./icos/name-card-icon.png",
    "./favicon.ico"
]

self.addEventListener('install', async e => {
    const cache = await caches.open(cacheName)
    await cache.addAll(cachables)
    return self.skipWaiting()
})

self.addEventListener('activate', e => {
    var cachesToKeep = [cacheName]

    //clear old cache
    e.waitUntil(
        caches.keys().then(function (keyList) {
            return Promise.all(keyList.map(function (key) {
                if (cachesToKeep.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }))
        })
    );

    self.clients.claim()
})

//cache first fetch
self.addEventListener('fetch', async e => {
    const url = new URL(e.request.url)
    const request = e.request

    if (url.origin === location.origin) {
        e.respondWith(caches.match(request).then((response) => {
                if (response) {
                    return response
                }

                return fetch(request).then(response => {
                    if (response.status == 404) {
                        return caches.match('./offline404.html')
                    }
                    
                    return response
                })
            })
            .catch(() => {
                return caches.match('./offline404.html')
            }))
    } else {
        e.respondWith(fetch(request).then((response) => {
                if(response.ok) {
                    return response
                }
                return caches.match('./offline404.html')
            })
            .catch(() => {
                return caches.match('./offline404.html')
            }))
    }
})

self.addEventListener('sync', e => {
    if (e.tag == "sync-name") {
        entries().then(entryList => {
            entryList.forEach(nameEntity => {
                fetch("/sendName", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: nameEntity[0],
                        name: nameEntity[1]
                    })
                }).then((res) => {
                    if (res.ok) {
                        res.json().then(data => {
                            del(data.id)
                        })
                    } else {
                        console.log(res)
                    }
                }).catch((err) => {
                    console.log("Couldn't send data to server :(")
                })
            })
        })
    }

    if(e.tag == "update-names") {
        fetchAndCacheLatestNames()
    }
})

self.addEventListener('push', function(e) {
    var data = {title: "RNG - Notification", body: "Default body", redirectUrl: "/"}

    if(e.data) {
        data = JSON.parse(e.data.text())
    }

    var options = {
      body: data.body,
      data: {
        redirectUrl: data.redirectUrl
      }
    };

    e.waitUntil(
      self.registration.showNotification(data.title, options)
    );
});

self.addEventListener('notificationclick', function(e) {
    clients.openWindow(e.notification.data.redirectUrl)
    e.notification.close()
});

function fetchAndCacheLatestNames() {
    console.log("Refreshing names.json")
    fetch('./names.json').then((response) => {
        if(response.ok) {
            caches.open(cacheName).then((cache) => {
                cache.delete('./names.json').then(() => {
                    cache.add('./names.json', response)
                })
            })
        }
    })
    .catch(() => {
        console.log("Couldn't fetch names, keeping old one")
    })
}