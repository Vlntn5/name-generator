var nameJson

window.onload = async function () {
    //enable supported features
    enableFeatures()

    //inital check if online
    checkOnline()

    checkPermission()
    registerSw()

    //check if subbed to notifs
    var reg = await navigator.serviceWorker.ready;
    var sub = await reg.pushManager.getSubscription()
    if(sub) {
        var bell = document.getElementById("bell-icon");
        bell.classList.replace("far", "fas")
    }

    setInterval(() => {
        navigator.serviceWorker.ready.then(function(swRegistration) {
            return swRegistration.sync.register('update-names');
        });
    }, 5 * 60 * 1000)
}

window.addEventListener('online',  checkOnline);
window.addEventListener('offline', checkOnline);

function checkOnline() {
    if(navigator.onLine) {
        document.getElementById("bell-icon").style.color = 'black'

        document.getElementById("submit-btn").disabled = false
        document.getElementById("submit-btn-old").disabled = false

        document.getElementById("submit-btn").style.backgroundColor = '#007bff'
        document.getElementById("submit-btn-old").style.backgroundColor = '#007bff'
    } else {
        document.getElementById("bell-icon").style.color = 'grey'

        document.getElementById("submit-btn").disabled = true
        document.getElementById("submit-btn-old").disabled = true

        document.getElementById("submit-btn").style.backgroundColor = 'grey'
        document.getElementById("submit-btn-old").style.backgroundColor = 'grey'
    }
}

function enableFeatures() {
    document.getElementById("generate-btns").style.display = 'flex'

    if(navigator.clipboard) {
        document.getElementById("pst-btn").style.display = 'unset'
        document.getElementById("pst-btn-old").style.display = 'unset'
        document.getElementById("cpy-btn").style.display = 'unset'
    }

    if(navigator.serviceWorker) {
        document.getElementById("submit-form").style.display = 'flex'
        document.getElementById("submit-form-old").style.display = 'none'

        if(Notification) {
            document.getElementById("bell-icon").style.display = 'unset'
        }
    }
}

async function registerPeriodicNewsCheck() {
    const registration = await navigator.serviceWorker.ready;
    try {
      await registration.periodicSync.register('update-names', {
        minInterval: 250,
        maxInterval: 300
      });
      console.log("Periodic Sync registered!");
    } catch {
      console.log('Periodic Sync could not be registered!');
    }
  }

async function copy() {
    navigator.permissions.query({name: 'clipboard-read'}).then(async (result) => {
        if(result.state == 'prompt' || result.state == 'granted') {
            try {
                var text = document.getElementById("generated-name").innerHTML;
                await navigator.clipboard.writeText(text.trim())
            } catch(e) {
                document.getElementById("cpy-btn").disabled = true
            }
        }
    })
}

async function paste() {
    navigator.permissions.query({name: 'clipboard-read'}).then(async (result) => {
        if(result.state == 'prompt' || result.state == 'granted') {
            try {
                var text = await navigator.clipboard.readText()
                document.getElementById("submit-name").value = text.trim()
            } catch(e) {
                document.getElementById("pst-btn").disabled = true
                document.getElementById("pst-btn-old").disabled = true
            }
        }
    })
}

async function generate() {
    if (!nameJson) {
        var response = await fetch('/names.json')
        nameJson = await response.json()
    }

    var nameIndex = Math.floor(Math.random() * nameJson.length)
    document.getElementById('generated-name').innerHTML = nameJson[nameIndex]
}

async function submit() {
    set(uuidv4(), document.getElementById('submit-name').value)
    document.getElementById('submit-name').value = ''
    navigator.serviceWorker.ready.then(function(swRegistration) {
        return swRegistration.sync.register('sync-name');
    });
}

async function toggleNotif() {
    var bell = document.getElementById("bell-icon");
    if(!navigator.onLine) return

    if(bell.classList.contains('fas')) {
        //disable notifs
        await unsubscribe()
        bell.classList.replace("fas", "far")
        bell.innerHTML = 'Notifications off'
    } else {
        //enable notifs
        Notification.requestPermission(async function(res) {
            if(res == "granted") {
                await subscribe()
                bell.classList.replace("far", "fas")
                bell.innerHTML = 'Notifications on'
            } else {
                bell.style.color = 'grey'
                bell.click = () => {console.log("User denied notifs")}
            }
            console.log("User decided :: " + res)
        })
    }
}

async function unsubscribe() {
    try {
        let reg = await navigator.serviceWorker.ready;
        let sub = await reg.pushManager.getSubscription();

        if (sub === null) {
            console.log("User isn't subscribed")
        } else {
            await fetch("/deleteSubscription", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                },
                body: JSON.stringify({ sub }),
            });
            await sub.unsubscribe()
        }
    } catch (error) {
        console.log(error);
    }
}

async function subscribe() {
    try {
        let reg = await navigator.serviceWorker.ready;
        let sub = await reg.pushManager.getSubscription();

        if (sub === null) {
            var publicKey =
                "BDMFSdlI3B9diMlf-L-_dKOyc30oOKtXKfgSyDRV80El4WZEuwbjJPyM22LPWWgfrx5YRxBAlPdI0SmSKgTR-8o";
            sub = await reg.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(publicKey)
            });
        } else {
            console.log("User alreay subscribed");
        }

        let res = await fetch("/saveSubscription", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
            body: JSON.stringify({ sub }),
        });
        if (res.ok) {
            console.log("Notifications on")
        }
    } catch (error) {
        console.log(error);
    }
}

function urlBase64ToUint8Array(base64String) {
    var padding = "=".repeat((4 - (base64String.length % 4)) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

async function checkPermission() {
    var permissionStatus = await navigator.permissions.query({
        name: 'clipboard-read'
    })
    console.log("Perm to read :: " + permissionStatus.state)
    if (permissionStatus.state != "denied") {
        document.getElementById("pst-btn").disabled = false
        document.getElementById("pst-btn-old").disabled = false
    }

    permissionStatus = await navigator.permissions.query({
        name: 'clipboard-write'
    })
    console.log("Perm to write :: " + permissionStatus.state)
    if (permissionStatus.state != "denied") {
        document.getElementById("cpy-btn").disabled = false
    }
}

async function registerSw() {
    if ('serviceWorker' in navigator) {
        try {
            await navigator.serviceWorker.register('./sw.js')
        } catch (e) {
            console.log("Registration failed.")
        }
    }
}

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }